#include <Trade\Trade.mqh>
CTrade trade;

double volume = 0.01;
input int shortSMA = 3;
input int longSMA = 20;
int initialStop = 1000;
int initialTake = 1000;
bool oneHour = false;

void OnTick()
  {
  
   double ATRarray[];
   int atr = iATR(_Symbol,PERIOD_M30,14);
   ArraySetAsSeries(ATRarray,true);
   CopyBuffer(atr,0,0,3,ATRarray);
   
   double myMovingAverageArray1[],myMovingAverageArray2[],myMovingAverageArray3[];
   int movingAverageDefinition1 = iMA (_Symbol,PERIOD_M30,9,0,MODE_EMA,PRICE_CLOSE);
   int movingAverageDefinition2 = iMA (_Symbol,PERIOD_M30,15,0,MODE_EMA,PRICE_CLOSE);
   
   ArraySetAsSeries(myMovingAverageArray1,true);
   ArraySetAsSeries(myMovingAverageArray2,true);
   CopyBuffer (movingAverageDefinition1,0,0,15,myMovingAverageArray1);
   CopyBuffer (movingAverageDefinition2,0,0,15,myMovingAverageArray2);
   
   
                  
   double Ask=NormalizeDouble(SymbolInfoDouble(_Symbol,SYMBOL_ASK),_Digits);
   double Bid=NormalizeDouble(SymbolInfoDouble(_Symbol,SYMBOL_BID),_Digits);
   
   int emaRange = int(myMovingAverageArray1[0]-myMovingAverageArray2[0])*100;
  
   datetime time = TimeLocal();
   string hourAndMinutes = TimeToString(time,TIME_MINUTES);
   
   bool PositionOpen = false;
   
    for(int i=PositionsTotal()-1;i>=0;i--){
      string symbol=PositionGetSymbol(i);
      if (symbol == _Symbol) 
        {
        PositionOpen = true;
        }
   }

   if( StringSubstr(hourAndMinutes,3,4)=="00" || StringSubstr(hourAndMinutes,3,4)=="30" ){
      oneHour = true;
   }
   if(ATRarray[0]<1000*_Point){volume=0.04;}
   if(ATRarray[0]>1000*_Point && ATRarray[0]<2000*_Point){volume=0.03;}
   if(ATRarray[0]>2000*_Point && ATRarray[0]<3000*_Point){volume=0.02;}
   if(ATRarray[0]>3000*_Point && ATRarray[0]<4000*_Point){volume=0.01;}
   
   if(ATRarray[0]<4000*_Point && PositionOpen == false && oneHour == true ){
     if( myMovingAverageArray1[1]>myMovingAverageArray2[1]&& myMovingAverageArray1[2]<myMovingAverageArray2[2] ){  
         initialStop = ATRarray[0];
         initialTake = ATRarray[0]; 
         BuyIt(Ask,volume); 
         PositionOpen = true;
         oneHour = false;
     }
    }
   
  
  }
void BuyIt(double Ask,double volume){
 trade.Buy(
    volume,
    NULL,
    Ask,
    (Ask-initialStop),//sl
    (Ask+initialTake),//tp
    NULL
  );
} 