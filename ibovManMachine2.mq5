#include <Trade\Trade.mqh>
CTrade trade;

double volume = 0.01;
int riskReward = 1; //Risk Reward 
int initialStop = 0;
int initialTake = 0;
int positionType;
double PositionVolume;

void OnTick()
  {
  
   double high[];
   ArraySetAsSeries(high,true);
   CopyHigh(_Symbol,PERIOD_M30,0,100,high);
   
   double low[];
   ArraySetAsSeries(low,true);
   CopyLow(_Symbol,PERIOD_M30,0,3,low);
  
   bool PositionOpen = false;
   datetime time = TimeLocal();
   string hourAndMinutes = TimeToString(time,TIME_MINUTES);
   
   double ATRarray[];
   int atr = iATR(_Symbol,PERIOD_M30,14);
   ArraySetAsSeries(ATRarray,true);
   CopyBuffer(atr,0,0,3,ATRarray);  
                  
   double Ask=NormalizeDouble(SymbolInfoDouble(_Symbol,SYMBOL_ASK),_Digits);
   double Bid=NormalizeDouble(SymbolInfoDouble(_Symbol,SYMBOL_BID),_Digits);
   
   if(ATRarray[0]<100*_Point){volume=0.10;}
   if(ATRarray[0]>100*_Point && ATRarray[0]<200*_Point){volume=0.09;}
   if(ATRarray[0]>200*_Point && ATRarray[0]<300*_Point){volume=0.08;}
   if(ATRarray[0]>300*_Point && ATRarray[0]<400*_Point){volume=0.07;}
   if(ATRarray[0]>400*_Point && ATRarray[0]<500*_Point){volume=0.06;}
   if(ATRarray[0]>500*_Point && ATRarray[0]<600*_Point){volume=0.05;}
   if(ATRarray[0]>600*_Point && ATRarray[0]<700*_Point){volume=0.04;}
   if(ATRarray[0]>700*_Point && ATRarray[0]<800*_Point){volume=0.03;}
   if(ATRarray[0]>800*_Point && ATRarray[0]<900*_Point){volume=0.02;}
   if(ATRarray[0]>900*_Point){volume=0.01;}
   
   initialStop = int(ATRarray[0]/10)*10;
   initialTake = (int(ATRarray[0]/10)*10)*riskReward;
   
   for(int i=PositionsTotal()-1;i>=0;i--){
      string symbol=PositionGetSymbol(i);
      if (symbol == _Symbol) 
        {
        PositionOpen = true;
       
        }
   }
   
   if(PositionOpen==false && hourAndMinutes=="12:01" && high[0]>high[1]){
      BuyIt(Ask,0.01);
      //SellIt(Bid,0.01);
   }
   if(PositionOpen==false && hourAndMinutes=="12:01" && low[0]<low[1]){
     // BuyIt(Ask,0.01);
      SellIt(Bid,0.01);
   }
   int atR = int(ATRarray[0]/10)*10; //normalizing atr
   Comment(
      "atr: ",atR,
      "\nvolume: ",volume,
      "\ntime: ",hourAndMinutes=="13:01"
   );
   CheckTrailingStop(Ask,Bid,int(ATRarray[0]/10)*10);
   
  
  }
void BuyIt(double Ask,double volume){
 trade.Buy(
    volume,
    NULL,
    Ask,
    (Ask-initialStop*_Point),//sl
    (Ask+initialTake*_Point),//tp
    NULL
  );
}
void SellIt(double Bid,double volume){
 trade.Sell(
    volume,
    NULL,
    Bid,
    (Bid+initialStop*_Point),//sl
    (Bid-initialTake*_Point),//tp
    NULL
  );
}
void CheckTrailingStop(double Ask  ,double Bid,double atr){
   for(int i=PositionsTotal()-1;i>=0;i--)
     {
      string symbol=PositionGetSymbol(i);
      if(_Symbol==symbol)
        {
         ulong PositionTicket=PositionGetInteger(POSITION_TICKET);
         double CurrentStopLoss=PositionGetDouble(POSITION_SL);  
         double CurrentTakeProfit=PositionGetDouble(POSITION_TP);  
         double PositionBuyPrice = PositionGetDouble(POSITION_PRICE_OPEN);
         double PositionProfit = PositionGetDouble(POSITION_PROFIT);
         double PositionVolume = PositionGetDouble(POSITION_VOLUME);
         int positionType = PositionGetInteger(POSITION_TYPE);
  
        
         if(positionType == 0){
           if( PositionVolume==0.01){
               BuyIt(Ask,volume);
            }
            if(CurrentStopLoss==0){
               trade.PositionModify(PositionTicket,PositionBuyPrice-atr*_Point,PositionBuyPrice+atr*_Point);  
            }
            if(Ask>PositionBuyPrice+(atr/2)){
               trade.PositionModify(PositionTicket,CurrentStopLoss+25*_Point,CurrentTakeProfit);  
            }  
         }else{ 
            if( PositionVolume==0.01){
               SellIt(Bid,volume);
            }
            if(CurrentStopLoss==0){
              trade.PositionModify(PositionTicket,PositionBuyPrice+atr*_Point,PositionBuyPrice-atr*_Point);  
            }
            if(Bid<PositionBuyPrice-(atr/2)){
               trade.PositionModify(PositionTicket,CurrentStopLoss-25*_Point,CurrentTakeProfit);  
            }
         }  
        }
     }

}
   