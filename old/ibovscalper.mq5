#include <Trade\Trade.mqh>
CTrade trade;


   bool buyTrigger = false;
void OnTick()
  {
  
   double Ask=NormalizeDouble(SymbolInfoDouble(_Symbol,SYMBOL_ASK),_Digits);
   double Bid=NormalizeDouble(SymbolInfoDouble(_Symbol,SYMBOL_BID),_Digits);
   double MidBandArray[];
   double UpperBandArray[];
   double LowerBandArray[]; 
   double myMovingAverageArray[];
      
   datetime time = TimeLocal();
   string hourAndMinutes = TimeToString(time,TIME_MINUTES);
   
   ArraySetAsSeries(MidBandArray,true);
   ArraySetAsSeries(UpperBandArray,true);
   ArraySetAsSeries(LowerBandArray,true);
   ArraySetAsSeries(myMovingAverageArray,true); 
   
   int BBDefinition = iBands(_Symbol,PERIOD_M1,20,0,2,PRICE_CLOSE);
   int movingAverageDefinition = iMA (_Symbol,_Period,20,0,MODE_SMA,PRICE_CLOSE);
   
   CopyBuffer(BBDefinition,0,0,3,MidBandArray);
   CopyBuffer(BBDefinition,1,0,3,UpperBandArray);
   CopyBuffer(BBDefinition,2,0,3,LowerBandArray);
   CopyBuffer(movingAverageDefinition,0,0,3,myMovingAverageArray);
   
   double midBandValue = MidBandArray[0];
   double upperBandValue = UpperBandArray[0];
   double lowerBandValue = LowerBandArray[0];
   double SlDifference = MathRound(upperBandValue-lowerBandValue);
   double volume = 1.00;
 
 //1000pt / 0.01 = 0.45c
 //100pt / 0.20 = 1.00E
 // 200 /100 = 2
 //SlDifference/100 = 2 
 //volume / 2

   if(Ask>upperBandValue && PositionsTotal()==0 &&  buyTrigger == true && hourAndMinutes<20){
   buyTrigger = false;
    trade.Buy(
          volume,
          NULL,
          Ask,
          Ask-500*_Point,
          Ask+250*_Point,
          NULL
        );
   }
 if(Bid<lowerBandValue){
   buyTrigger = true;
   /*   trade.Sell(
          volume,
          NULL,
          Bid,
          Bid+500*_Point,
          Bid-250*_Point,
          NULL
        );
   */
   }
    for(int i=PositionsTotal()-1;i>=0;i--){
      string symbol=PositionGetSymbol(i);
      if(_Symbol==symbol)
        {
         ulong PositionTicket=PositionGetInteger(POSITION_TICKET);
         double CurrentStopLoss=PositionGetDouble(POSITION_SL);  
         double PositionBuyPrice = PositionGetDouble(POSITION_PRICE_OPEN);
         double PositionProfit = PositionGetDouble(POSITION_PROFIT);
         double PositionVolume = PositionGetDouble(POSITION_VOLUME);
         if(hourAndMinutes>20){
              trade.PositionClose(PositionTicket);
         }
        }
     }
   
   
   
   
   
   
   
   Comment(
      "hour: ",(hourAndMinutes>16),"\n",
      "UPPER: ",upperBandValue,"\n",
      "MID: ",midBandValue,"\n",
      "LOWER",lowerBandValue,"\n",
      "upper-lower: ",SlDifference,"\n",
      "LOWER",buyTrigger,"\n"
   );
   
  
   
   
   
  }
