#include <Trade\Trade.mqh>
CTrade trade;

double volume = 0.1;


void OnTick()
  {
   double Ask=NormalizeDouble(SymbolInfoDouble(_Symbol,SYMBOL_ASK),_Digits);
   double Bid=NormalizeDouble(SymbolInfoDouble(_Symbol,SYMBOL_BID),_Digits);
   
   
   datetime time = TimeLocal();
   string hourAndMinutes = TimeToString(time,TIME_MINUTES);
   
   bool PositionOpen = false;
   
    for(int i=PositionsTotal()-1;i>=0;i--){
      string symbol=PositionGetSymbol(i);
      if (symbol == _Symbol) 
        {
        PositionOpen = true;
        }
   }
   Comment("time: ",hourAndMinutes);
   if(PositionOpen == false && StringSubstr(hourAndMinutes,3,4)=="43"){
       PositionOpen = true;
       BuyIt(Ask,volume);
       //Comment("PositionOpen: ",PositionOpen);
     }
    CheckTrailingStop(Ask);
     
  }
void BuyIt(double Ask,double volume){
 trade.Buy(
    volume,
    NULL,
    Ask,
    (Ask-2000*_Point),//sl
    (Ask+2000*_Point),//tp
    NULL
  );
}
void CheckTrailingStop(double Ask){
   double SL=NormalizeDouble(Ask-1000*_Point,_Digits);
   for(int i=PositionsTotal()-1;i>=0;i--)
     {
      string symbol=PositionGetSymbol(i);
      if(_Symbol==symbol)
        {
         ulong PositionTicket=PositionGetInteger(POSITION_TICKET);
         double CurrentStopLoss=PositionGetDouble(POSITION_SL);  
         double PositionBuyPrice = PositionGetDouble(POSITION_PRICE_OPEN);
         double PositionProfit = PositionGetDouble(POSITION_PROFIT);
         double TP = PositionBuyPrice+2000*_Point;
          if(PositionBuyPrice+300*_Point<Ask){
            trade.PositionModify(PositionTicket,(PositionBuyPrice+100*_Point),TP);
         }
        }
     }

}