#include <Trade\Trade.mqh>
CTrade trade;

double volume = 0.5;
double initialStop = 950*_Point;
double initialTake = 1050*_Point;
input int longEMA = 20;

void OnTick()
  {
   double highestPrice;
   double high[];
   ArraySetAsSeries(high,true);
   CopyHigh(_Symbol,PERIOD_M30,0,100,high);
   highestPrice = ArrayMaximum(high,0,100);
   
   double lowestPrice;
   double low[];
   ArraySetAsSeries(low,true);
   CopyLow(_Symbol,PERIOD_M30,0,3,low);
   lowestPrice = ArrayMinimum(low,0,3);
  
  
   double ATRarray[];
   int atr = iATR(_Symbol,PERIOD_M30,14);
   ArraySetAsSeries(ATRarray,true);
   CopyBuffer(atr,0,0,3,ATRarray);
  
   double Ask=NormalizeDouble(SymbolInfoDouble(_Symbol,SYMBOL_ASK),_Digits);
   double Bid=NormalizeDouble(SymbolInfoDouble(_Symbol,SYMBOL_BID),_Digits);
   
  /* if(ATRarray[0]<100*_Point){volume=1.0;}
   if(ATRarray[0]>100*_Point && ATRarray[0]<200*_Point){volume=0.8;}
   if(ATRarray[0]>200*_Point && ATRarray[0]<300*_Point){volume=0.6;}
   if(ATRarray[0]>300*_Point && ATRarray[0]<400*_Point){volume=0.4;}
   if(ATRarray[0]>400*_Point && ATRarray[0]<500*_Point){volume=0.2;}*/
   
   datetime time = TimeLocal();
   string hourAndMinutes = TimeToString(time,TIME_MINUTES);
   
   bool PositionOpen = false;
   
    for(int i=PositionsTotal()-1;i>=0;i--){
      string symbol=PositionGetSymbol(i);
      if (symbol == _Symbol) 
        {
        PositionOpen = true;
        }
        
   }
   Comment("time: ",hourAndMinutes,"\n",
   "high 0: ",high[0],
   "\nhigh 1: ",high[1],
   "\nhigh 2: ",high[2],
   "\nAsk: ",StringSubstr(IntegerToString(Ask),2)
   
   );
   double price = StringSubstr(IntegerToString(Ask),2);
   double price2 = StringSubstr(IntegerToString(Ask),3);
   if( price=="000" && PositionOpen==false || price2=="000" && PositionOpen==false ){
       PositionOpen = true;
       //initialStop=ATRarray[0];
       //initialTake=ATRarray[0]+100*_Point;
       BuyIt(Ask,volume);
     }
  
     
  }
void BuyIt(double Ask,double volume){
 trade.Buy(
    volume,
    NULL,
    Ask,
    (Ask-initialStop),//sl
    (Ask+initialTake),//tp
    NULL
  );
}

