 #include <Trade\Trade.mqh>
 CTrade trade;
void OnTick()
  {
   double myMovingAverageArray1[],myMovingAverageArray2[],bullArray[];
   double Ask=NormalizeDouble(SymbolInfoDouble(_Symbol,SYMBOL_ASK),_Digits);
   double Bid=NormalizeDouble(SymbolInfoDouble(_Symbol,SYMBOL_ASK),_Digits);
   int movingAverageDefinition1 = iMA (_Symbol,_Period,1,0,MODE_EMA,PRICE_CLOSE);
   int movingAverageDefinition2 = iMA (_Symbol,_Period,100,0,MODE_EMA,PRICE_CLOSE);
   
   
   MqlRates PriceInfo[];
   ArraySetAsSeries(PriceInfo,true);
   
   int bullsDefinition = iBullsPower (_Symbol,_Period,10);
   ArraySetAsSeries(myMovingAverageArray1,true);
   ArraySetAsSeries(myMovingAverageArray2,true);
   CopyBuffer (movingAverageDefinition1,0,0,5,myMovingAverageArray1);
   CopyBuffer (movingAverageDefinition2,0,0,5,myMovingAverageArray2);
   CopyBuffer (bullsDefinition,0,0,5,bullArray);
  
   if(
   (PositionsTotal()==0)&&
   (myMovingAverageArray1[0]>myMovingAverageArray2[0])&&
   (bullArray[0]>100)&&
   (myMovingAverageArray1[1]<myMovingAverageArray2[1])
   ){
      Comment("BUY");
         trade.Buy(
          0.12,
          NULL,
          Ask,
          Ask-750*_Point,
          Ask+1000*_Point,
          NULL
          );
   } 
   
  if(
   (myMovingAverageArray1[0]<myMovingAverageArray2[0])&&
   (myMovingAverageArray1[1]>myMovingAverageArray2[1])
   ){
     //Comment("SELL");
         //trade.Sell(
          //0.01,
          //NULL,
          //Bid,
          //Bid+1000*_Point,
          //Ask+1000*_Point,
          //NULL
         // );
   } 
        CheckTrailingStop(Ask); 
   
  }

 void CheckTrailingStop(double Ask){
   double SL=NormalizeDouble(Ask-1000*_Point,_Digits);
   for(int i=PositionsTotal()-1;i>=0;i--)
     {
      string symbol=PositionGetSymbol(i); 
      if(_Symbol==symbol)
        {
         ulong PositionTicket=PositionGetInteger(POSITION_TICKET);
         double CurrentStopLoss=PositionGetDouble(POSITION_SL);  
         double PositionBuyPrice = PositionGetDouble(POSITION_PRICE_OPEN);
         if(CurrentStopLoss<SL){
            trade.PositionModify(PositionTicket,(CurrentStopLoss+10*_Point),0 );
         }
         if(Ask==(PositionBuyPrice+300*_Point))
           {
            trade.PositionModify(PositionTicket,(PositionBuyPrice+50*_Point),0);
           }
        if(Ask==(PositionBuyPrice+1500*_Point))
        {
         trade.PositionModify(PositionTicket,(PositionBuyPrice+1000*_Point),0);
        }
           
        }
     }
}
