 #include <Trade\Trade.mqh>
 CTrade trade;
void OnTick()
  {
   double myMovingAverageArray1[],myMovingAverageArray2[],bullArray[];
   double Ask=NormalizeDouble(SymbolInfoDouble(_Symbol,SYMBOL_ASK),_Digits);
   double Bid=NormalizeDouble(SymbolInfoDouble(_Symbol,SYMBOL_ASK),_Digits);
   int movingAverageDefinition1 = iMA (_Symbol,_Period,10,0,MODE_EMA,PRICE_CLOSE);
   int movingAverageDefinition2 = iMA (_Symbol,_Period,300,0,MODE_EMA,PRICE_CLOSE);
   
   
   MqlRates PriceInfo[];
   ArraySetAsSeries(PriceInfo,true);
   
   int bullsDefinition = iBullsPower (_Symbol,_Period,10);
   ArraySetAsSeries(myMovingAverageArray1,true);
   ArraySetAsSeries(myMovingAverageArray2,true);
   CopyBuffer (movingAverageDefinition1,0,0,5,myMovingAverageArray1);
   CopyBuffer (movingAverageDefinition2,0,0,5,myMovingAverageArray2);
   CopyBuffer (bullsDefinition,0,0,5,bullArray);
  
   if(
   (PositionsTotal()==0)&&
   (myMovingAverageArray1[0]>myMovingAverageArray2[0])&&
   (bullArray[0]>10)&&
   (myMovingAverageArray1[1]<myMovingAverageArray2[1])
   ){
      Comment("BUY");
         trade.Buy(
          0.01,
          NULL,
          Ask,
          Ask-100*_Point,
          Ask+100*_Point,
          NULL
          );
   } 
   
  
        CheckTrailingStop(Ask,myMovingAverageArray2[0]); 
   
  }

 void CheckTrailingStop(double Ask){
   double SL=NormalizeDouble(Ask-100*_Point,_Digits);
   for(int i=PositionsTotal()-1;i>=0;i--)
     {
      string symbol=PositionGetSymbol(i); 
      if(_Symbol==symbol)
        {
         ulong PositionTicket=PositionGetInteger(POSITION_TICKET);
         double CurrentStopLoss=PositionGetDouble(POSITION_SL);  
         double PositionBuyPrice = PositionGetDouble(POSITION_PRICE_OPEN);
         if(CurrentStopLoss<SL){
            trade.PositionModify(PositionTicket,(CurrentStopLoss+1*_Point),0 );
         }
         if(Ask==(PositionBuyPrice+100*_Point))
           {
            trade.PositionModify(PositionTicket,(PositionBuyPrice+20*_Point),0);
           }
        if(Ask==(PositionBuyPrice+300*_Point))
        {
         trade.PositionModify(PositionTicket,(PositionBuyPrice+250*_Point),0);
        }
           
        }
     }
}
