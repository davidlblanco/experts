#include <Trade\Trade.mqh>
CTrade trade;

double volume = 0.01;
input int shortSMA = 15;
input int longSMA = 70;
int initialStop = 1000;
int initialTake = 1000;
bool oneHour = false;

void OnTick()
  {
  
   /*double bears[];
   int bearsDefinition = iBearsPower (_Symbol,PERIOD_M30,13);
   CopyBuffer (bearsDefinition,0,0,5,bears);*/
  
  
   double ATRarray[];
   int atr = iATR(_Symbol,PERIOD_M30,14);
   ArraySetAsSeries(ATRarray,true);
   CopyBuffer(atr,0,0,3,ATRarray);
   
   double myMovingAverageArray1[],myMovingAverageArray2[],myMovingAverageArray3[];
   int movingAverageDefinition1 = iMA (_Symbol,PERIOD_M30,15,0,MODE_EMA,PRICE_CLOSE);
   int movingAverageDefinition2 = iMA (_Symbol,PERIOD_M30,70,0,MODE_EMA,PRICE_CLOSE);
   //int movingAverageDefinition3 = iMA (_Symbol,PERIOD_M30,100,0,MODE_EMA,PRICE_CLOSE);
   
   ArraySetAsSeries(myMovingAverageArray1,true);
   ArraySetAsSeries(myMovingAverageArray2,true);
   //ArraySetAsSeries(myMovingAverageArray3,true);
   CopyBuffer (movingAverageDefinition1,0,0,15,myMovingAverageArray1);
   CopyBuffer (movingAverageDefinition2,0,0,15,myMovingAverageArray2);
  // CopyBuffer (movingAverageDefinition3,0,0,15,myMovingAverageArray3);
   
   
                  
   double Ask=NormalizeDouble(SymbolInfoDouble(_Symbol,SYMBOL_ASK),_Digits);
   double Bid=NormalizeDouble(SymbolInfoDouble(_Symbol,SYMBOL_BID),_Digits);
   
   int emaRange = int(myMovingAverageArray1[0]-myMovingAverageArray2[0])*100;
  
   datetime time = TimeLocal();
   string hourAndMinutes = TimeToString(time,TIME_MINUTES);
   
   bool PositionOpen = false;
   
    for(int i=PositionsTotal()-1;i>=0;i--){
      string symbol=PositionGetSymbol(i);
      if (symbol == _Symbol) 
        {
        PositionOpen = true;
        }
   }
   Comment("time: ",hourAndMinutes,
   "\nSMA: ",myMovingAverageArray2[0], 
   //int(highestPrice),
   "\nAsk: ",Ask,
   "\nemaRange: ",emaRange,
   "\nSL: ",initialStop,
   "\ninitialTake: ",initialTake,
   "\noneHour: ",oneHour/*,
   "\nbears: ",bears[3]*/
   );
   if( StringSubstr(hourAndMinutes,3,4)=="00" || StringSubstr(hourAndMinutes,3,4)=="30" ){
      oneHour = true;
   }
   if(ATRarray[0]<1000*_Point){volume=0.04;}
   if(ATRarray[0]>1000*_Point && ATRarray[0]<2000*_Point){volume=0.03;}
   if(ATRarray[0]>2000*_Point && ATRarray[0]<3000*_Point){volume=0.02;}
   if(ATRarray[0]>3000*_Point && ATRarray[0]<4000*_Point){volume=0.01;}
   
   if(ATRarray[0]<4000*_Point && PositionOpen == false && oneHour == true ){
     if( Ask>myMovingAverageArray1[0] ){  
         initialStop = emaRange;
         initialTake = emaRange;
       //  SellIt(Bid,volume); 
      // PositionOpen = true;
       //oneHour = false;
     }
     if( myMovingAverageArray1[1]<myMovingAverageArray2[1] && myMovingAverageArray1[2]>myMovingAverageArray2[2] ){
         initialStop = ATRarray[0];
         initialTake = ATRarray[0]; 
         SellIt(Bid,volume); 
         PositionOpen = true;
         oneHour = false;
     }
    }
   

     //CheckTrailingStop(Ask, emaRange,myMovingAverageArray1[0],myMovingAverageArray2[0]);
  
  }
void BuyIt(double Ask,double volume){
 trade.Buy(
    volume,
    NULL,
    Ask,
    (Ask-initialStop*_Point),//sl
    (Ask+initialTake*_Point),//tp
    NULL
  );
}
void SellIt(double Bid,double volume){
 trade.Sell(
    volume,
    NULL,
    Bid,
    (Bid+initialStop),//sl
    (Bid-initialTake),//tp
    NULL
  );
}

void CheckTrailingStop(double Ask  ,double emaRange,double sellStop,double buyStop){
   for(int i=PositionsTotal()-1;i>=0;i--)
     {
      string symbol=PositionGetSymbol(i);
      if(_Symbol==symbol)
        {
         ulong PositionTicket=PositionGetInteger(POSITION_TICKET);
         double CurrentStopLoss=PositionGetDouble(POSITION_SL);
         double CurrentTakeProfit=PositionGetDouble(POSITION_TP);  
         double PositionBuyPrice = PositionGetDouble(POSITION_PRICE_OPEN);
         double PositionProfit = PositionGetDouble(POSITION_PROFIT);
         double PositionVolume = PositionGetDouble(POSITION_VOLUME);
         int positionType = PositionGetInteger(POSITION_TYPE);
         if(positionType==0){ 
            if(PositionBuyPrice+(CurrentTakeProfit-PositionBuyPrice)/2*_Point<Ask && CurrentStopLoss<PositionBuyPrice){
               trade.PositionModify(PositionTicket,(PositionBuyPrice+250*_Point),0);   
            }if(buyStop>CurrentStopLoss && CurrentStopLoss>PositionBuyPrice){
               trade.PositionModify(PositionTicket,(buyStop),0);
            }
         }else{
            if(PositionBuyPrice-(CurrentTakeProfit+PositionBuyPrice)/2*_Point<Ask && CurrentStopLoss>PositionBuyPrice){
               trade.PositionModify(PositionTicket,(PositionBuyPrice-250*_Point),0); //tenho que salvar emaRange em outra variavel  
            }if(sellStop<CurrentStopLoss && CurrentStopLoss<PositionBuyPrice){
               trade.PositionModify(PositionTicket,(sellStop),0);
            }
         }
             
        }
     }

}
  