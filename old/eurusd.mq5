#include <Trade\Trade.mqh>
CTrade trade;
void OnTick()
  {
   double Ask = NormalizeDouble(SymbolInfoDouble(_Symbol,SYMBOL_ASK),_Digits);
   double Bid = NormalizeDouble(SymbolInfoDouble(_Symbol,SYMBOL_BID),_Digits);
   bool PositionOpen = false;
   for(int i=PositionsTotal()-1;i>=0;i--){
      string symbol=PositionGetSymbol(i);
      if (symbol == _Symbol) 
        {
        PositionOpen = true;
        }
   }
   Comment("PositionOpen: ",PositionOpen);
   if(PositionOpen == false){
      BuyIt(Ask);
      PositionOpen = true;
   }
   CheckTrailingStop(Bid);
  }
void BuyIt(double Bid){
     // if you take a 2500pt = 4,51 | 5000pt = 9.03
      trade.Sell(
       0.01,
       NULL,
       Bid,
       Bid+1000*_Point,
       Bid-1000*_Point,
       NULL
     );
}
void CheckTrailingStop(double Bid){
   double SL=NormalizeDouble(Bid+1000*_Point,_Digits);
   for(int i=PositionsTotal()-1;i>=0;i--)
     {
      string symbol=PositionGetSymbol(i);
      if(_Symbol==symbol)
        {
         ulong PositionTicket=PositionGetInteger(POSITION_TICKET);
         double CurrentStopLoss=PositionGetDouble(POSITION_SL);  
         double PositionBuyPrice = PositionGetDouble(POSITION_PRICE_OPEN);
         double TP = PositionBuyPrice+100*_Point;
         //Comment("Current Profit: ",(CurrentStopLoss-PositionBuyPrice)/100*0.54); //need to ajust this to usaind profit
          if(CurrentStopLoss>SL){
            trade.PositionModify(PositionTicket,(CurrentStopLoss-10*_Point),0);
         }

        }
     }

}