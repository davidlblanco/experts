#include <Trade\Trade.mqh>
CTrade trade;

double volume = 0.01;


void OnTick()
  {
   double Ask=NormalizeDouble(SymbolInfoDouble(_Symbol,SYMBOL_ASK),_Digits);
   double Bid=NormalizeDouble(SymbolInfoDouble(_Symbol,SYMBOL_BID),_Digits);
   
   double highestPrice;
   double high[];
   ArraySetAsSeries(high,true);
   CopyHigh(_Symbol,PERIOD_M30,0,100,high);
   highestPrice = ArrayMaximum(high,0,100);
   
   double lowestPrice;
   double low[];
   ArraySetAsSeries(low,true);
   CopyLow(_Symbol,PERIOD_M30,0,3,low);
   lowestPrice = ArrayMinimum(low,0,3);
   double lowNumber = low[1];    
   
   
   datetime time = TimeLocal();
   string hourAndMinutes = TimeToString(time,TIME_MINUTES);
   
   bool PositionOpen = false;
   
    for(int i=PositionsTotal()-1;i>=0;i--){
      string symbol=PositionGetSymbol(i);
      if (symbol == _Symbol) 
        {
        PositionOpen = true;
        }
   }
   Comment("time: ",hourAndMinutes,"\n",
         "lownumber: ",lowNumber
   );
   if(PositionOpen == false && StringSubstr(hourAndMinutes,3,4)=="13"){
       PositionOpen = true;
       BuyIt(Ask,volume);
       //Comment("PositionOpen: ",PositionOpen);
     }
    CheckTrailingStop(Ask,lowNumber);
     
  }
void BuyIt(double Ask,double volume){
 trade.Buy(
    volume,
    NULL,
    Ask,
    (Ask-5000*_Point),//sl
    (Ask+5000*_Point),//tp
    NULL
  );
}
void CheckTrailingStop(double Ask,double lowNumber){
   double SL=NormalizeDouble(Ask-1000*_Point,_Digits);
   for(int i=PositionsTotal()-1;i>=0;i--)
     {
      string symbol=PositionGetSymbol(i);
      if(_Symbol==symbol)
        {
         ulong PositionTicket=PositionGetInteger(POSITION_TICKET);
         double CurrentStopLoss=PositionGetDouble(POSITION_SL);  
         double PositionBuyPrice = PositionGetDouble(POSITION_PRICE_OPEN);
         double PositionProfit = PositionGetDouble(POSITION_PROFIT);
         double PositionVolume = PositionGetDouble(POSITION_VOLUME);
         double TP = PositionBuyPrice+5000*_Point;
         if(PositionBuyPrice+1000*_Point<Ask && CurrentStopLoss<PositionBuyPrice){
               trade.PositionModify(PositionTicket,(PositionBuyPrice+250*_Point),0);
           }
        
         if(  CurrentStopLoss>PositionBuyPrice){
               trade.PositionModify(PositionTicket,lowNumber,0);
         } 
      
   
        }
     }

}
  