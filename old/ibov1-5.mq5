#include <Trade\Trade.mqh>
CTrade trade;

double volume = 0.01;
double bet = 1;
double volumeBet = volume;
bool BuySellToggle = true;

void OnTick()
  {
   double Ask=NormalizeDouble(SymbolInfoDouble(_Symbol,SYMBOL_ASK),_Digits);
   double Bid=NormalizeDouble(SymbolInfoDouble(_Symbol,SYMBOL_BID),_Digits);
   
   
   datetime time = TimeLocal();
   string hourAndMinutes = TimeToString(time,TIME_MINUTES);
   
   bool PositionOpen = false;
   
    for(int i=PositionsTotal()-1;i>=0;i--){
      string symbol=PositionGetSymbol(i);
      ulong PositionTicket=PositionGetInteger(POSITION_TICKET);
      if (symbol == _Symbol) 
        {
        PositionOpen = true;
         if(hourAndMinutes>21){
            trade.PositionClose(PositionTicket);
         }
        }
   }
  
   if(BuySellToggle==true && PositionsTotal()==0){
       BuyIt(Ask,volumeBet);
   }if(BuySellToggle==false && PositionsTotal()==0){
      SellIt(Bid,volumeBet);
   }
 
     CheckTrailingStop(Ask,Bid);
  }
void BuyIt(double Ask,double volume){
 trade.Buy(
    volume,
    NULL,
    Ask,
    (Ask-500*_Point),//sl
    (Ask+500*_Point),//tp
    NULL
  );
}
void SellIt(double Bid,double volume){
 trade.Sell(
    volume,
    NULL,
    Bid,
    (Bid+500*_Point),//sl
    (Bid-500*_Point),//tp
    NULL
  );
}
void CheckTrailingStop(double Ask, double Bid){
   double SL=NormalizeDouble(Ask-1000*_Point,_Digits);
   for(int i=PositionsTotal()-1;i>=0;i--)
     {
      string symbol=PositionGetSymbol(i);
      if(_Symbol==symbol)
        {
         ulong PositionTicket=PositionGetInteger(POSITION_TICKET);
         double CurrentStopLoss=PositionGetDouble(POSITION_SL);  
         double PositionEntry = PositionGetDouble(POSITION_PRICE_OPEN);
         double PositionProfit = PositionGetDouble(POSITION_PROFIT);
         double PositionVolume = PositionGetDouble(POSITION_VOLUME);
         double PositionType = PositionGetInteger(POSITION_TYPE);
         
         double TP = 500*_Point;
         if(PositionType==0){
            Comment("buy");
           
 
            if(PositionEntry+100<Ask){
              trade.PositionModify(PositionTicket,(PositionEntry+50*_Point),PositionEntry+TP);
              BuySellToggle=true;
            }else{
              BuySellToggle=false; 
            }
         } 
         if(PositionType==1){
            Comment("sell"); 
        
            if(PositionEntry-100>Bid){
               trade.PositionModify(PositionTicket,(PositionEntry-50*_Point),PositionEntry-TP);
               BuySellToggle=false;
            }else{
               BuySellToggle=true;
            }
         }
         
          
        }
     }

}