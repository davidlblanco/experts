 #include <Trade\Trade.mqh>
 CTrade trade;
void OnTick()
  {
   double myMovingAverageArray1[],myMovingAverageArray2[],myRsi14[];
   double Ask=NormalizeDouble(SymbolInfoDouble(_Symbol,SYMBOL_ASK),_Digits);
   
   double Bid=NormalizeDouble(SymbolInfoDouble(_Symbol,SYMBOL_ASK),_Digits);
   int movingAverageDefinition1 = iMA (_Symbol,_Period,1,0,MODE_EMA,PRICE_CLOSE);
   int movingAverageDefinition2 = iMA (_Symbol,_Period,3,0,MODE_EMA,PRICE_CLOSE);
   int myRsiDefinition = iRSI (_Symbol,_Period,14,PRICE_CLOSE);
   ArraySetAsSeries(myMovingAverageArray1,true);
   ArraySetAsSeries(myMovingAverageArray2,true);
   ArraySetAsSeries(myRsi14,true);
   CopyBuffer (movingAverageDefinition1,0,0,3,myMovingAverageArray1);
   CopyBuffer (movingAverageDefinition2,0,0,3,myMovingAverageArray2);
   CopyBuffer (myRsiDefinition,0,0,3,myRsi14);
   MqlRates PriceInfo[];
   ArraySetAsSeries(PriceInfo,true);
   double myRsiValue=NormalizeDouble(myRsi14[0],2);
   
   
   if(
      (PositionsTotal()==0)&&
      (myRsi14[0]=30)&&
      (myRsi14[1]<30)&&    
      (myMovingAverageArray1[0]>myMovingAverageArray2[0]) 
      )
     {
         trade.Buy(
          0.01,
          NULL,
          Ask,
          Ask-200*_Point,
          Ask+200*_Point,
          NULL
          );
      }
    
   
     
  
   
   if(
   (myMovingAverageArray1[0]>myMovingAverageArray2[0])
   &&
   (myMovingAverageArray1[1]<myMovingAverageArray2[1])
   ){
      Comment("BUY");
   }  if(
   (myMovingAverageArray1[0]<myMovingAverageArray2[0])
   &&
   (myMovingAverageArray1[1]>myMovingAverageArray2[1])
   ){
      Comment("SELL");
   }
   
  }

