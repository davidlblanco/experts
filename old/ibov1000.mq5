#include <Trade\Trade.mqh>
CTrade trade;
void OnTick()
  {
   double Ask=NormalizeDouble(SymbolInfoDouble(_Symbol,SYMBOL_ASK),_Digits);
   double Bid=NormalizeDouble(SymbolInfoDouble(_Symbol,SYMBOL_BID),_Digits);
   
   if(
   (PositionsTotal()==0)
   )
     {
       trade.Buy(
          0.12,
          NULL,
          Ask,
          Ask-1000*_Point,
          Ask+1500*_Point,
          NULL
        );
     }
     CheckTrailingStop(Ask,Bid);
  }
void CheckTrailingStop(double Ask,double Bid){
   double SL=NormalizeDouble(Ask-1000*_Point,_Digits);
   for(int i=PositionsTotal()-1;i>=0;i--)
     {
      string symbol=PositionGetSymbol(i);
      if(_Symbol==symbol)
        {
         ulong PositionTicket=PositionGetInteger(POSITION_TICKET);
         double CurrentStopLoss=PositionGetDouble(POSITION_SL);  
         double PositionBuyPrice = PositionGetDouble(POSITION_PRICE_OPEN);
         double PositionProfit = PositionGetDouble(POSITION_PROFIT);
         double TP = PositionBuyPrice+750*_Point;
        // Comment("Current Profit: ",(CurrentStopLoss-PositionBuyPrice)/100*0.54);
        // Comment("Current Profit: ",PositionProfit); //need to ajust this to usaind profit
          if(CurrentStopLoss<SL){
            trade.PositionModify(PositionTicket,(CurrentStopLoss+10*_Point),0);
         }
         if(PositionProfit>3.00)
           {
           
             trade.Sell(
                0.11,
                NULL,
                Bid,
                0,
                0,
                NULL
              );
           }

        }
     }

}