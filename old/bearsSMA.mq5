 #include <Trade\Trade.mqh>
 CTrade trade;
void OnTick()
  {
   double myMovingAverageArray1[],myMovingAverageArray2[],bearsArray[];
   double Ask=NormalizeDouble(SymbolInfoDouble(_Symbol,SYMBOL_ASK),_Digits);
   double Bid=NormalizeDouble(SymbolInfoDouble(_Symbol,SYMBOL_ASK),_Digits);
   int movingAverageDefinition1 = iMA (_Symbol,_Period,1,0,MODE_EMA,PRICE_CLOSE);
   int movingAverageDefinition2 = iMA (_Symbol,_Period,100,0,MODE_EMA,PRICE_CLOSE);
   
   
   MqlRates PriceInfo[];
   ArraySetAsSeries(PriceInfo,true);
   
   int bearsDefinition = iBearsPower (_Symbol,_Period,10);
   ArraySetAsSeries(myMovingAverageArray1,true);
   ArraySetAsSeries(myMovingAverageArray2,true);
   CopyBuffer (movingAverageDefinition1,0,0,5,myMovingAverageArray1);
   CopyBuffer (movingAverageDefinition2,0,0,5,myMovingAverageArray2);
   CopyBuffer (bearsDefinition,0,0,5,bearsArray);
  
   if(
   (PositionsTotal()==0)&&
   (myMovingAverageArray1[0]<myMovingAverageArray2[0])&&
   (bearsArray[0]<-1000)&&
   (myMovingAverageArray1[1]>myMovingAverageArray2[1])
   ){
         
          trade.Sell(
          0.12,
          NULL,
          Bid,
          Bid+1000*_Point,
          Bid-1000*_Point,
          NULL
          );
   } 
   
  
        CheckTrailingStop(Bid); 
   
  }

 void CheckTrailingStop(double Bid){
   double SL=NormalizeDouble(Bid+1000*_Point,_Digits);
   for(int i=PositionsTotal()-1;i>=0;i--)
     {
      string symbol=PositionGetSymbol(i); 
      if(_Symbol==symbol)
        {
         ulong PositionTicket=PositionGetInteger(POSITION_TICKET);
         double CurrentStopLoss=PositionGetDouble(POSITION_SL);  
         double PositionBuyPrice = PositionGetDouble(POSITION_PRICE_OPEN);
         if(CurrentStopLoss>SL){
            trade.PositionModify(PositionTicket,(CurrentStopLoss-10*_Point),0 );
         }
         if(Bid==(PositionBuyPrice-300*_Point))
           {
            trade.PositionModify(PositionTicket,(PositionBuyPrice-50*_Point),0);
           }
        if(Bid==(PositionBuyPrice-1500*_Point))
        {
         trade.PositionModify(PositionTicket,(PositionBuyPrice-1000*_Point),0);
        }
           
        }
     }
}
