#include <Trade\Trade.mqh>
CTrade trade;
void OnTick()
  {
   double Ask=NormalizeDouble(SymbolInfoDouble(_Symbol,SYMBOL_ASK),_Digits);
   if(PositionsTotal()<1)
    trade.Buy(
          0.10,
          NULL,
          Ask,
          (Ask-1000*_Point),
          (Ask+1000*_Point),
          NULL
          );
    checkBreakEvenStop(Ask);
  }
 
 void checkBreakEvenStop(double Ask){
   for(int i=PositionsTotal()-1;i>=0;i--)
     {
      string symbol=PositionGetSymbol(i);
      if(_Symbol==symbol)
        {
         ulong PositionTicket = PositionGetInteger(POSITION_TICKET);
         double PositionBuyPrice = PositionGetDouble(POSITION_PRICE_OPEN);
         if(Ask==(PositionBuyPrice+300*_Point))
           {
            trade.PositionModify(PositionTicket,(PositionBuyPrice+50*_Point),0);
           }
        }
     }
 }
        