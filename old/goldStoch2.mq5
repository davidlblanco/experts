#include <Trade\Trade.mqh>
CTrade trade;

double volume = 0.01;// Quantidade de contratos
int initSL = 500;// Stop loss inicial
input int breakEven = 300;// Break even
input int Beven = 50;// Quanto o sl fica a frente do preço depois do Break even
input int initialTS = 10;//Quanto anda o trailing stop 
input int TSdistance = 300;//Distância maxima do stop pro preço depois do break even
bool oneHour = false;

void OnTick()
  {
  
   double green[], red[];
   int stoch = iStochastic(_Symbol,PERIOD_M30,30,9,9,MODE_EMA,STO_CLOSECLOSE);
   ArraySetAsSeries(green,true);
   ArraySetAsSeries(red,true);
   CopyBuffer(stoch,0,0,3,green);
   CopyBuffer(stoch,1,0,3,red);
  
    
   double ATRarray[];
   int atr = iATR(_Symbol,PERIOD_M30,14);
   ArraySetAsSeries(ATRarray,true);
   CopyBuffer(atr,0,0,3,ATRarray);
                  
   double Ask=NormalizeDouble(SymbolInfoDouble(_Symbol,SYMBOL_ASK),_Digits);
   double Bid=NormalizeDouble(SymbolInfoDouble(_Symbol,SYMBOL_BID),_Digits);
   
  
   datetime time = TimeLocal();
   string hourAndMinutes = TimeToString(time,TIME_MINUTES);
   
   bool PositionOpen = false;
   
    for(int i=PositionsTotal()-1;i>=0;i--){
      string symbol=PositionGetSymbol(i);
      if (symbol == _Symbol) 
        {
        PositionOpen = true;
        }
   }
   if(ATRarray[0]<100*_Point){volume=0.05;}
   if(ATRarray[0]>100*_Point && ATRarray[0]<200*_Point){volume=0.04;}
   if(ATRarray[0]>200*_Point && ATRarray[0]<300*_Point){volume=0.03;}
   if(ATRarray[0]>300*_Point && ATRarray[0]<400*_Point){volume=0.02;}
   if(ATRarray[0]>400*_Point && ATRarray[0]<500*_Point){volume=0.01;}
   if( StringSubstr(hourAndMinutes,3,4)=="00" || StringSubstr(hourAndMinutes,3,4)=="30" ){
      oneHour = true;
   }
   
   if( PositionOpen == false && oneHour == true ){
     if( green[1]>80 && green[2]<80  ){
         Comment("Buy"); 
         initSL = ATRarray[0]*2;
         BuyIt(Ask,volume); 
         PositionOpen = true;
         oneHour = false;
     }
     if( green[1]>20 && green[2]<20 ){  
         Comment("Sell");
         initSL = ATRarray[0]*2;
         SellIt(Bid,volume); 
         PositionOpen = true;
         oneHour = false;
     }
    }
     

    // CheckTrailingStop(Ask, Bid);
  
  }
void BuyIt(double Ask,double volume){
 trade.Buy(
    volume,
    NULL,
    Ask,
    Ask-initSL,//sl
    Ask+initSL,//tp
    NULL
  );
}
void SellIt(double Bid,double volume){
 trade.Sell(
    volume,
    NULL,
    Bid,
    Bid+initSL,//sl
    Bid-initSL,//tp
    NULL
  );
}

void CheckTrailingStop(double Ask,double Bid){
   double SL=NormalizeDouble(Ask-TSdistance*_Point,_Digits);
   double SLS=NormalizeDouble(Bid+TSdistance*_Point,_Digits);
   for(int i=PositionsTotal()-1;i>=0;i--)
     {
      string symbol=PositionGetSymbol(i);
      if(_Symbol==symbol)
        {
         ulong PositionTicket=PositionGetInteger(POSITION_TICKET);
         double CurrentStopLoss=PositionGetDouble(POSITION_SL);
         double CurrentTakeProfit=PositionGetDouble(POSITION_TP);  
         double PositionBuyPrice = PositionGetDouble(POSITION_PRICE_OPEN);
         double PositionProfit = PositionGetDouble(POSITION_PROFIT);
         double PositionVolume = PositionGetDouble(POSITION_VOLUME);
         int positionType = PositionGetInteger(POSITION_TYPE);
        
         if(positionType==0){ 
            if(PositionBuyPrice+breakEven*_Point<Ask && CurrentStopLoss<PositionBuyPrice){
               trade.PositionModify(PositionTicket,(PositionBuyPrice+Beven*_Point),0);
            }
            if(CurrentStopLoss<SL && CurrentStopLoss>PositionBuyPrice ){
               trade.PositionModify(PositionTicket,(CurrentStopLoss+initialTS*_Point),0);
            }
         }else{
            if( PositionBuyPrice-breakEven*_Point>Bid && CurrentStopLoss>PositionBuyPrice){
                trade.PositionModify(PositionTicket,(PositionBuyPrice-Beven*_Point),0);
            }
            if(CurrentStopLoss>SLS && CurrentStopLoss<PositionBuyPrice ){
               trade.PositionModify(PositionTicket,(CurrentStopLoss-initialTS*_Point),0);
            }
         }
             
        }
     }

}
  