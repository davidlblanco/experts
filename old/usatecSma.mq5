#include <Trade\Trade.mqh>
CTrade trade;
void OnTick()
  {
  
   double myMovingAverageArray1[],myMovingAverageArray2[];
   
   int movingAverageDefinition1 = iMA (_Symbol,PERIOD_M30,1,0,MODE_EMA,PRICE_CLOSE);
   int movingAverageDefinition2 = iMA (_Symbol,PERIOD_M30,3,0,MODE_EMA,PRICE_CLOSE);
   
   double Ask = NormalizeDouble(SymbolInfoDouble(_Symbol,SYMBOL_ASK),_Digits);
   
   MqlRates PriceInfo[];
   ArraySetAsSeries(PriceInfo,true);
   
   
   ArraySetAsSeries(myMovingAverageArray1,true);
   ArraySetAsSeries(myMovingAverageArray2,true);
   CopyBuffer (movingAverageDefinition1,0,0,5,myMovingAverageArray1);
   CopyBuffer (movingAverageDefinition2,0,0,5,myMovingAverageArray2);
   
   
   bool PositionOpen = false;
   for(int i=PositionsTotal()-1;i>=0;i--){
      string symbol=PositionGetSymbol(i);
      if (symbol == _Symbol) 
        {
        PositionOpen = true;
        }
   }
   Comment("PositionOpen: ",PositionOpen);
   if(PositionOpen == false && (myMovingAverageArray1[0]>myMovingAverageArray2[0]) && (myMovingAverageArray1[1]<myMovingAverageArray2[1])){
      BuyIt(Ask);
      PositionOpen = true;
   }
   CheckTrailingStop(Ask);
  }
void BuyIt(double Ask){
     // if you take a 2500pt = 4,51 | 5000pt = 9.03
      trade.Buy(
       0.01,
       NULL,
       Ask,
       Ask-5000*_Point,
       Ask+5000*_Point,
       NULL
     );
}
void CheckTrailingStop(double Ask){
   double SL=NormalizeDouble(Ask-5000*_Point,_Digits);
   for(int i=PositionsTotal()-1;i>=0;i--)
     {
      string symbol=PositionGetSymbol(i);
      if(_Symbol==symbol)
        {
         ulong PositionTicket=PositionGetInteger(POSITION_TICKET);
         double CurrentStopLoss=PositionGetDouble(POSITION_SL);  
         double PositionBuyPrice = PositionGetDouble(POSITION_PRICE_OPEN);
         double TP = PositionBuyPrice+100*_Point;
         //Comment("Current Profit: ",(CurrentStopLoss-PositionBuyPrice)/100*0.54); //need to ajust this to usaind profit
          if(CurrentStopLoss<SL){
            trade.PositionModify(PositionTicket,(CurrentStopLoss+25*_Point),0);
         }

        }
     }

}