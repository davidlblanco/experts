#include <Trade\Trade.mqh>
CTrade trade;

//first Gain Loss Volume
double initGain = 0.50;
double initLoss = -0.50;
double initVol = 0.01;

double maxVol = 0.16;

double gain = initGain;
double loss = initLoss;
double volume = initVol;


void OnTick()
  {
   double Ask=NormalizeDouble(SymbolInfoDouble(_Symbol,SYMBOL_ASK),_Digits);
   double Bid=NormalizeDouble(SymbolInfoDouble(_Symbol,SYMBOL_BID),_Digits);
   bool firstOpen = true;
   if(PositionsTotal()==0 && firstOpen == true){
       firstOpen = false;
       BuyIt(Ask,volume);
       Comment("firstOpen: ",firstOpen);
     }
   for(int i=PositionsTotal()-1;i>=0;i--){
      string symbol=PositionGetSymbol(i);
      if(_Symbol==symbol)
        {
         ulong PositionTicket=PositionGetInteger(POSITION_TICKET);
         double CurrentStopLoss=PositionGetDouble(POSITION_SL);  
         double PositionBuyPrice = PositionGetDouble(POSITION_PRICE_OPEN);
         double PositionProfit = PositionGetDouble(POSITION_PROFIT);
         double PositionVolume = PositionGetDouble(POSITION_VOLUME);
         
     
         if(PositionProfit >= gain || PositionProfit <= loss && volume == maxVol){
              trade.PositionClose(PositionTicket);
              gain = initGain;
              loss = initLoss;
              volume = initVol;
              BuyIt(Ask,volume);
         }
         if(PositionProfit <= loss){
              gain = gain*2;
              loss = loss*2;
              volume = volume*2;
              trade.PositionClose(PositionTicket);
              BuyIt(Ask,volume);
         }
         
        

        }
     }
     
  }
void BuyIt(double Ask,double volume){
// if you take a 100pt = 4,74 | 200pt = 9,48
 trade.Buy(
    volume,
    NULL,
    Ask,
    0,
    0,
    NULL
  );
}
