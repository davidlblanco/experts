#include <Trade\Trade.mqh>
CTrade trade;
void OnTick()
  {
   double SMA1[];
   double SMA2[];
   double bulls[];
   double Ask=NormalizeDouble(SymbolInfoDouble(_Symbol,SYMBOL_ASK),_Digits);
   
   int movingAverageDefinition1 = iMA (_Symbol,_Period,2,0,MODE_SMA,PRICE_CLOSE);
   int movingAverageDefinition2 = iMA (_Symbol,_Period,20,0,MODE_SMA,PRICE_CLOSE);
   int bullsDefinition = iBullsPower (_Symbol,_Period,13);
   
   ArraySetAsSeries(SMA1,true); 
   ArraySetAsSeries(SMA2,true); 
   
   CopyBuffer(movingAverageDefinition1,0,0,3,SMA1);
   CopyBuffer(movingAverageDefinition2,0,0,3,SMA2);
   CopyBuffer (bullsDefinition,0,0,5,bulls);
   if(
   (PositionsTotal()==0)
   )
     {
       trade.Buy(
          0.12,
          NULL,
          Ask,
          Ask-1000*_Point,
          Ask+1500*_Point,
          NULL
        );
     }
     CheckTrailingStop(Ask);
  }
void CheckTrailingStop(double Ask){
   double SL=NormalizeDouble(Ask-1000*_Point,_Digits);
   for(int i=PositionsTotal()-1;i>=0;i--)
     {
      string symbol=PositionGetSymbol(i);
      if(_Symbol==symbol)
        {
         ulong PositionTicket=PositionGetInteger(POSITION_TICKET);
         double CurrentStopLoss=PositionGetDouble(POSITION_SL);  
         double PositionBuyPrice = PositionGetDouble(POSITION_PRICE_OPEN);
         double TP = PositionBuyPrice+750*_Point;
         Comment("Current Profit: ",(CurrentStopLoss-PositionBuyPrice)/100*0.54); //need to ajust this to usaind profit
          if(CurrentStopLoss<SL){
            trade.PositionModify(PositionTicket,(CurrentStopLoss+10*_Point),0);
         }

        }
     }

}