#include <Trade\Trade.mqh>
CTrade trade;

double volume = 0.01;
input int highMa = 3;
input int lowMa = 3;
input int stop = 1000;
input int take = 1000;

void OnTick()
  {
   double myMovingAverageArray1[],myMovingAverageArray2[];
   int movingAverageDefinition1 = iMA (_Symbol,PERIOD_M30,highMa,0,MODE_EMA,PRICE_HIGH);
   int movingAverageDefinition2 = iMA (_Symbol,PERIOD_M30,lowMa,0,MODE_EMA,PRICE_LOW);
   
   
   ArraySetAsSeries(myMovingAverageArray1,true);
   ArraySetAsSeries(myMovingAverageArray2,true);
   CopyBuffer (movingAverageDefinition1,0,0,15,myMovingAverageArray1);
   CopyBuffer (movingAverageDefinition2,0,0,15,myMovingAverageArray2);
   
                  
   double Ask=NormalizeDouble(SymbolInfoDouble(_Symbol,SYMBOL_ASK),_Digits);
   double Bid=NormalizeDouble(SymbolInfoDouble(_Symbol,SYMBOL_BID),_Digits);
   
   
   datetime time = TimeLocal();
   string hourAndMinutes = TimeToString(time,TIME_MINUTES);
   
   bool PositionOpen = false;
   
    for(int i=PositionsTotal()-1;i>=0;i--){
      string symbol=PositionGetSymbol(i);
      if (symbol == _Symbol) 
        {
        PositionOpen = true;
        }
   }
   Comment("time: ",hourAndMinutes,"\n"
   );
   if( myMovingAverageArray1[0]>Ask && PositionOpen == false ){      
      BuyIt(Ask,volume);
      PositionOpen = true;
    } 
      if(myMovingAverageArray2[0]>Bid && PositionOpen == false){
        PositionOpen = true;
        SellIt(Bid,volume);
      }

      //CheckTrailingStop(Ask, Bid, myMovingAverageArray1[0]);
  
  }
void BuyIt(double Ask,double volume){
 trade.Buy(
    volume,
    NULL,
    Ask,
    (Ask-stop*_Point),//sl
    (Ask+take*_Point),//tp
    NULL
  );
}
void SellIt(double Bid,double volume){
 trade.Sell(
    volume,
    NULL,
    Bid,
    (Bid+stop*_Point),//sl
    (Bid-take*_Point),//tp
    NULL
  );
}


void CheckTrailingStop(double Ask ,double Bid ,double myMovingAverageArray2){
   double SL=NormalizeDouble(myMovingAverageArray2,_Digits);
   double SLS=NormalizeDouble(myMovingAverageArray2,_Digits);
   for(int i=PositionsTotal()-1;i>=0;i--)
     {
      string symbol=PositionGetSymbol(i);
      if(_Symbol==symbol)
        {
         ulong PositionTicket=PositionGetInteger(POSITION_TICKET);
         double CurrentStopLoss=PositionGetDouble(POSITION_SL);  
         double PositionBuyPrice = PositionGetDouble(POSITION_PRICE_OPEN);
         double PositionProfit = PositionGetDouble(POSITION_PROFIT);
         double PositionVolume = PositionGetDouble(POSITION_VOLUME);
         double TP = PositionBuyPrice+2500*_Point;
         double TPS = PositionBuyPrice-5000*_Point;
         int positionType = PositionGetInteger(POSITION_TYPE);
         if(positionType==0){
            if(PositionBuyPrice+1000*_Point<Ask && CurrentStopLoss<PositionBuyPrice){
               trade.PositionModify(PositionTicket,(PositionBuyPrice+250*_Point),0);   
            }
            if( CurrentStopLoss<SL && CurrentStopLoss>PositionBuyPrice){
               trade.PositionModify(PositionTicket,(CurrentStopLoss+25*_Point),0);
           }
         } 
         if(positionType==1){
            if(PositionBuyPrice-1000*_Point>Bid && CurrentStopLoss>PositionBuyPrice){
               trade.PositionModify(PositionTicket,(PositionBuyPrice-250*_Point),0);   
            }
            if( CurrentStopLoss>SLS && CurrentStopLoss<PositionBuyPrice){
               trade.PositionModify(PositionTicket,(CurrentStopLoss-25*_Point),0);
            } 
         }
         
         
   
        }
     }

}
  