 #include <Trade\Trade.mqh>
 CTrade trade;
void OnTick()
  {
   double myMovingAverageArray1[],myMovingAverageArray2[],bullArray[],bearsArray[];
   double Ask=NormalizeDouble(SymbolInfoDouble(_Symbol,SYMBOL_ASK),_Digits);
   double Bid=NormalizeDouble(SymbolInfoDouble(_Symbol,SYMBOL_ASK),_Digits);
   int movingAverageDefinition1 = iMA (_Symbol,_Period,1,0,MODE_EMA,PRICE_CLOSE);
   int movingAverageDefinition2 = iMA (_Symbol,_Period,20,0,MODE_EMA,PRICE_CLOSE);
   
   
   MqlRates PriceInfo[];
   ArraySetAsSeries(PriceInfo,true);
   
   int bullsDefinition = iBullsPower (_Symbol,_Period,10);
   int bearsDefinition = iBearsPower (_Symbol,_Period,10);
   ArraySetAsSeries(myMovingAverageArray1,true);
   ArraySetAsSeries(myMovingAverageArray2,true);
   CopyBuffer (movingAverageDefinition1,0,0,5,myMovingAverageArray1);
   CopyBuffer (movingAverageDefinition2,0,0,5,myMovingAverageArray2);
   CopyBuffer (bullsDefinition,0,0,5,bullArray);
   CopyBuffer (bearsDefinition,0,0,5,bearsArray);
  
   if(
   (PositionsTotal()==0)&&
   (myMovingAverageArray1[0]>myMovingAverageArray2[0])&&
   (bullArray[0]>100)&&
   (myMovingAverageArray1[1]<myMovingAverageArray2[1])
   ){
         trade.Sell(
          0.12,
          NULL,
          Bid,
          Bid+200*_Point,
          Bid-100*_Point,
          NULL
          );
   } 
   
  if(
   (PositionsTotal()==0)&&
   (myMovingAverageArray1[0]<myMovingAverageArray2[0])&&
   (bearsArray[0]>100)&&
   (myMovingAverageArray1[1]>myMovingAverageArray2[1])
   ){   trade.Buy(
          0.12,
          NULL,
          Ask,
          Ask-200*_Point,
          Ask+100*_Point,
          NULL
          );
     
   } 
   
  }
