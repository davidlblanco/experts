void OnTick()
  {
  //create an array for several prices
   double myMovingAverageArray[];
   // define proprieties of the moving average
   int movingAverageDefinition = iMA (_Symbol,_Period,20,0,MODE_SMA,PRICE_CLOSE);
   //sort the price array from the current candle downards
   ArraySetAsSeries(myMovingAverageArray,true); 
   //Define EA, one line, current candle, 3 candles store result
   CopyBuffer(movingAverageDefinition,0,0,3,myMovingAverageArray);
   double myMovingAverageValue=myMovingAverageArray[0];
   //output current ea on the chart
   Comment("MovingAverageValue: ",myMovingAverageValue);
  }
