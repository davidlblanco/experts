#include <Trade\Trade.mqh>
CTrade trade;
void OnTick()
  {
  
   double macdArray[];
   
   int MacdDefinition = iMACD (_Symbol,PERIOD_D1,12,26,9,PRICE_CLOSE);
   
   double Ask = NormalizeDouble(SymbolInfoDouble(_Symbol,SYMBOL_ASK),_Digits);
   double Bid=NormalizeDouble(SymbolInfoDouble(_Symbol,SYMBOL_BID),_Digits);
   
   MqlRates PriceInfo[];
   ArraySetAsSeries(macdArray,true);
   
   
   CopyBuffer (MacdDefinition,0,0,5,macdArray);
   
   
   bool PositionOpen = false;
   for(int i=PositionsTotal()-1;i>=0;i--){
      string symbol=PositionGetSymbol(i);
      if (symbol == _Symbol) 
        {
        PositionOpen = true;
        }
   }
   Comment("PositionOpen: ",PositionOpen);
   if(PositionOpen == false && (macdArray[0]>0)){
      BuyIt(Ask);
      PositionOpen = true;
   }
   if(PositionOpen == false && (macdArray[0]<0)){
      SellIt(Bid);
      PositionOpen = true;
   }
   CheckTrailingStopBuy(Ask);
   CheckTrailingStopSell(Bid);
  }
void BuyIt(double Ask){
     // if you take a 2500pt = 4,51 | 5000pt = 9.03
      trade.Buy(
       0.01,
       NULL,
       Ask,
       Ask-5000*_Point,
       Ask+5000*_Point,
       NULL
     );
}void SellIt(double Bid){
     // if you take a 2500pt = 4,51 | 5000pt = 9.03
      trade.Sell(
       0.01,
       NULL,
       Bid,
       Bid-5000*_Point,
       Bid+5000*_Point,
       NULL
     );
}
void CheckTrailingStopBuy(double Ask){
   double SL=NormalizeDouble(Ask-5000*_Point,_Digits);
   for(int i=PositionsTotal()-1;i>=0;i--)
     {
      string symbol=PositionGetSymbol(i);
      if(_Symbol==symbol)
        {
         ulong PositionTicket=PositionGetInteger(POSITION_TICKET);
         double CurrentStopLoss=PositionGetDouble(POSITION_SL);  
         double PositionBuyPrice = PositionGetDouble(POSITION_PRICE_OPEN);
         double TP = PositionBuyPrice+100*_Point;
         //Comment("Current Profit: ",(CurrentStopLoss-PositionBuyPrice)/100*0.54); //need to ajust this to usaind profit
          if(CurrentStopLoss<SL){
            trade.PositionModify(PositionTicket,(CurrentStopLoss+25*_Point),0);
         }

        }
     }

}
void CheckTrailingStopSell(double Bid){
   double SL=NormalizeDouble(Bid+5000*_Point,_Digits);
   for(int i=PositionsTotal()-1;i>=0;i--)
     {
      string symbol=PositionGetSymbol(i);
      if(_Symbol==symbol)
        {
         ulong PositionTicket=PositionGetInteger(POSITION_TICKET);
         double CurrentStopLoss=PositionGetDouble(POSITION_SL);  
         double PositionBuyPrice = PositionGetDouble(POSITION_PRICE_OPEN);
         double TP = PositionBuyPrice+100*_Point;
         //Comment("Current Profit: ",(CurrentStopLoss-PositionBuyPrice)/100*0.54); //need to ajust this to usaind profit
          if(CurrentStopLoss>SL){
            trade.PositionModify(PositionTicket,(CurrentStopLoss-25*_Point),0);
         }

        }
     }

}