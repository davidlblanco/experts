 #include <Trade\Trade.mqh>
 CTrade trade;
void OnTick()
  {
   double myMovingAverageArray1[],myMovingAverageArray2[],myMovingAverageArray3[];
   double Ask=NormalizeDouble(SymbolInfoDouble(_Symbol,SYMBOL_ASK),_Digits);
   double Bid=NormalizeDouble(SymbolInfoDouble(_Symbol,SYMBOL_ASK),_Digits);
   int movingAverageDefinition1 = iMA (_Symbol,_Period,3,0,MODE_EMA,PRICE_CLOSE);
   int movingAverageDefinition2 = iMA (_Symbol,_Period,300,0,MODE_EMA,PRICE_CLOSE);
   int movingAverageDefinition3 = iMA (_Symbol,_Period,1,0,MODE_EMA,PRICE_CLOSE);
   ArraySetAsSeries(myMovingAverageArray1,true);
   ArraySetAsSeries(myMovingAverageArray2,true);
   CopyBuffer (movingAverageDefinition1,0,0,3,myMovingAverageArray1);
   CopyBuffer (movingAverageDefinition2,0,0,3,myMovingAverageArray2);
   CopyBuffer (movingAverageDefinition3,0,0,3,myMovingAverageArray3);
   MqlRates PriceInfo[];
   ArraySetAsSeries(PriceInfo,true);
 
   if(
   ((myMovingAverageArray1[0]+2500*_Point)<myMovingAverageArray2[0])&&
   (myMovingAverageArray1[0]<myMovingAverageArray3[0])&&
   (myMovingAverageArray1[1]>myMovingAverageArray3[1])
   ){
      Comment("BUY");
         trade.Buy(
          0.01,
          NULL,
          Ask,
          Ask-1000*_Point,
          //Ask+1000*_Point,
          NULL
          );
   } 
   
 
           CheckTrailingStop(Ask);
   
  
   
  }

   void CheckTrailingStop(double Ask){
   double SL=NormalizeDouble(Ask-1000*_Point,_Digits);
   for(int i=PositionsTotal()-1;i>=0;i--)
     {
      string symbol=PositionGetSymbol(i); 
      if(_Symbol=="Bra50Oct19")
        {
         ulong PositionTicket=PositionGetInteger(POSITION_TICKET);
         double CurrentStopLoss=PositionGetDouble(POSITION_SL);
         if(CurrentStopLoss<SL){
            trade.PositionModify(PositionTicket,(CurrentStopLoss+10*_Point),0 );
         }
        }
     }
}
