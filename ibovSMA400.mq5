#include <Trade\Trade.mqh>
CTrade trade;

double volume = 0.5;
input int longEMA = 20;

void OnTick()
  {
   /*double myMovingAverageArray1[],myMovingAverageArray2[];
   int movingAverageDefinition1 = iMA (_Symbol,PERIOD_M30,3,0,MODE_EMA,PRICE_CLOSE);
   int movingAverageDefinition2 = iMA (_Symbol,PERIOD_M30,longEMA,0,MODE_EMA,PRICE_CLOSE);
   
   
   ArraySetAsSeries(myMovingAverageArray1,true);
   ArraySetAsSeries(myMovingAverageArray2,true);
   CopyBuffer (movingAverageDefinition1,0,0,5,myMovingAverageArray1);
   CopyBuffer (movingAverageDefinition2,0,0,5,myMovingAverageArray2);*/
   
   double highestPrice;
   double high[];
   ArraySetAsSeries(high,true);
   CopyHigh(_Symbol,PERIOD_M30,0,100,high);
   highestPrice = ArrayMaximum(high,0,4);
   
   double lowestPrice;
   double low[];
   ArraySetAsSeries(low,true);
   CopyLow(_Symbol,PERIOD_M30,0,3,low);
   lowestPrice = ArrayMinimum(low,0,4);
   
   
   double Ask=NormalizeDouble(SymbolInfoDouble(_Symbol,SYMBOL_ASK),_Digits);
   double Bid=NormalizeDouble(SymbolInfoDouble(_Symbol,SYMBOL_BID),_Digits);
   
   
   datetime time = TimeLocal();
   string hourAndMinutes = TimeToString(time,TIME_MINUTES);
   
   bool PositionOpen = false;
   
    for(int i=PositionsTotal()-1;i>=0;i--){
      string symbol=PositionGetSymbol(i);
      if (symbol == _Symbol) 
        {
        PositionOpen = true;
        }
        
   }
   Comment("time: ",hourAndMinutes,"\n"
   
   );
   if( PositionOpen == false ){
       PositionOpen = true;
       BuyIt(Ask,volume);
     }
  
     
  }
void BuyIt(double Ask,double volume){
 trade.Buy(
    volume,
    NULL,
    Ask,
    (Ask-200*_Point),//sl
    (Ask+200*_Point),//tp
    NULL
  );
}